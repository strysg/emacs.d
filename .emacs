;;;; .emacs 8 dic 2018
;;;; .emacs 21 de Enero de 2020
;;;; .emacs 29 de Enero de 2021
;;;; .emacs 7 de Abril de 2021
;;;; .emacs 6 de Agosto de 2022
;;;; .emacs 20 de Octubre de 2022
;;;; .emacs 5 de Marzo de 2024
;;;; .emacs 10 de Junio de 2024

;; (package-initialize)
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)

;;; instalacion de paquetes automatizada
;;;; Fuente: https://www.reddit.com/r/emacs/comments/4fqu0a/automatically_install_packages_on_startup/

;; === SETUP ===
;; (package-initialize)

;; === CUSTOM CHECK FUNCTION ===
(defun ensure-package-installed (&rest packages)
  "Assure every package is installed, ask for installation if it’s not.
   Return a list of installed packages or nil for every skipped package."
  (mapcar
   (lambda (package)
     (unless (package-installed-p package)
       (package-install package)))
     packages)
)

;; === List my packages ===
;; simply add package names to the list
(ensure-package-installed
 'auto-complete
 'rjsx-mode
 'yasnippet
 'hydra
 'flycheck
 'company
 'avy
 'which-key
 'dap-mode
 'json-mode
 'projectile
 'js2-mode
 'dockerfile-mode
 ;; 'python-mode
 'multiple-cursors
 'web-mode
 'vue-mode
 'jedi
 'neotree
 'treemacs
 'treemacs-all-the-icons
 'elpy
 'flycheck
 'py-autopep8
 'blacken
 'latex-preview-pane
 'typescript-mode
 'tide
 'prettier-js
 ;;'plantuml-mod
 'rainbow-delimiters
 'yaml-mode
 'use-package
 'terraform-mode
 ;;'highlight-indentation-mode
 ;; ... etc
)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector
   (vector "#212121" "#B71C1C" "#558b2f" "#FFA000" "#2196f3" "#4527A0" "#00796b" "#FAFAFA"))
 '(company-quickhelp-color-background "#4F4F4F")
 '(company-quickhelp-color-foreground "#DCDCCC")
 '(custom-safe-themes
   '("a20d3bf4dc8f6266e3b8d800cb48fadf28b08c9fbbd67f9ebc6620586066df8e" "f079ef5189f9738cf5a2b4507bcaf83138ad22d9c9e32a537d61c9aae25502ef" "f74e8d46790f3e07fbb4a2c5dafe2ade0d8f5abc9c203cd1c29c7d5110a85230" "3b8284e207ff93dfc5e5ada8b7b00a3305351a3fb222782d8033a400a48eca48" "90a6f96a4665a6a56e36dec873a15cbedf761c51ec08dd993d6604e32dd45940" "f149d9986497e8877e0bd1981d1bef8c8a6d35be7d82cba193ad7e46f0989f6a" default))
 '(fci-rule-color "#ECEFF1")
 '(hl-sexp-background-color "#efebe9")
 '(js-indent-level 2)
 '(js2-basic-offset 2)
 '(nrepl-message-colors
   '("#CC9393" "#DFAF8F" "#F0DFAF" "#7F9F7F" "#BFEBBF" "#93E0E3" "#94BFF3" "#DC8CC3"))
 '(package-selected-packages
   '(ag blamer feature-mode font-utils default-font-presets yasnippet projectile hydra flycheck company avy which-key dap-mode zenburn-theme json-mode))
 '(pdf-view-midnight-colors '("#DCDCCC" . "#383838"))
 '(plantuml-jar-path "/home/rgarcia/Misc/emacs/plantuml.jar" t)
 '(show-paren-mode t)
 '(vc-annotate-background nil)
 '(vc-annotate-color-map
   '((20 . "#B71C1C")
     (40 . "#FF5722")
     (60 . "#FFA000")
     (80 . "#558b2f")
     (100 . "#00796b")
     (120 . "#2196f3")
     (140 . "#4527A0")
     (160 . "#B71C1C")
     (180 . "#FF5722")
     (200 . "#FFA000")
     (220 . "#558b2f")
     (240 . "#00796b")
     (260 . "#2196f3")
     (280 . "#4527A0")
     (300 . "#B71C1C")
     (320 . "#FF5722")
     (340 . "#FFA000")
     (360 . "#558b2f")))
 '(vc-annotate-very-old-color nil))
;;
;; para javascript mode js2-mode
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
;; hace que en lugar de insertar tabs se inserten espacios en blanco
(add-hook 'js2-mode-hook
          '(lambda () (progn
                        (set-variable 'indent-tabs-mode nil))))

;; algunos hooks para web-mode
(defun my-web-mode-hook ()
  "Hooks for Web mode."
  ;;  HTML element offset indentation 
  (setq web-mode-markup-indent-offset 2)
  ;;  CSS offset indentation
  (setq web-mode-css-indent-offset 2)
  ;;  Script/code offset indentation (for JavaScript, Java, PHP, Ruby, Go, VBScript, Python, etc.) 
  (setq web-mode-code-indent-offset 2)
  ;; Tabs nil
  (set-variable 'indent-tabs-mode nil)
  )

(add-hook 'web-mode-hook  'my-web-mode-hook)
;; resaltar columna actual
(setq web-mode-enable-current-column-highlight t)


;; para react
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
;;(setq package-selected-packages '(lsp-mode yasnippet lsp-treemacs helm-lsp projectile hydra flycheck company avy which-key helm-xref dap-mode zenburn-theme json-mode))
(setq package-selected-packages '(yasnippet projectile hydra flycheck company avy which-key dap-mode zenburn-theme json-mode))
(when (cl-find-if-not #'package-installed-p package-selected-packages)
  (package-refresh-contents)
  (mapc #'package-install package-selected-packages))
;;(load-theme 'zenburn t)
;; (helm-mode)
;; (require 'helm-xref)
;;(define-key global-map [remap find-file] #'helm-find-files)
;;(define-key global-map [remap execute-extended-command] #'helm-M-x)
;;(define-key global-map [remap switch-to-buffer] #'helm-mini)
(which-key-mode)
;(add-hook 'prog-mode-hook #'lsp)
(setq gc-cons-threshold (* 100 1024 1024)
      read-process-output-max (* 1024 1024)
      company-idle-delay 0.0
      company-minimum-prefix-length 1
      create-lockfiles nil ;; lock files will kill `npm start'
      )
      ;;lsp-headerline-breadcrumb-enable t)
(with-eval-after-load 'lsp-mode
  (require 'dap-chrome)
  ;(add-hook 'lsp-mode-hook #'lsp-enable-which-key-integration)
  (yas-global-mode))



;; asociando archivos con extensiones automaticamente a web-mode
(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.vue\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.scss\\'" . web-mode))

(setq web-mode-engines-alist
      '(("php"    . "\\.phtml\\'")
        ("blade"  . "\\.blade\\.")
	("vue"  . "\\.vue\\.")
	)
      )

;; agregando autcompletado de ac-js2 a js2-mode
;; (add-hook 'js-mode-hook 'js2-minor-mode)
;; (add-hook 'js2-mode-hook 'ac-js2-mode)
;; ;; js2-mode provides 4 level of syntax highlighting. They are

;; ;;     0 or a negative value means none.
;; ;;     1 adds basic syntax highlighting.
;; ;;     2 adds highlighting of some Ecma built-in properties.
;; ;;     3 adds highlighting of many Ecma built-in functions.
;; (setq js2-highlight-level 3)

;; activar ac-autocomplete en algunos modos por defecto
(require 'auto-complete)
(global-auto-complete-mode t)

;;(require 'highlight-indentation-mode)

;;;; .emacs 8 dic 2018
;;;; .emacs 21 de Enero de 2020
;;;; .emacs 29 de Enero de 2021
;;;; .emacs 7 de Abril de 2021


;; (package-initialize)
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)

;;; instalacion de paquetes automatizada
;;;; Fuente: https://www.reddit.com/r/emacs/comments/4fqu0a/automatically_install_packages_on_startup/

;; === SETUP ===
(package-initialize)

;; === CUSTOM CHECK FUNCTION ===
(defun ensure-package-installed (&rest packages)
  "Assure every package is installed, ask for installation if it’s not.
   Return a list of installed packages or nil for every skipped package."
  (mapcar
   (lambda (package)
     (unless (package-installed-p package)
       (package-install package)))
     packages)
)

;;
;; para javascript mode js2-mode
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
;; hace que en lugar de insertar tabs se inserten espacios en blanco
(add-hook 'js2-mode-hook
          '(lambda () (progn
                        (set-variable 'indent-tabs-mode nil))))
;; algunos hooks para web-mode
(defun my-web-mode-hook ()
  "Hooks for Web mode."
  ;;  HTML element offset indentation 
  (setq web-mode-markup-indent-offset 2)
  ;;  CSS offset indentation
  (setq web-mode-css-indent-offset 2)
  ;;  Script/code offset indentation (for JavaScript, Java, PHP, Ruby, Go, VBScript, Python, etc.) 
  (setq web-mode-code-indent-offset 2)
  ;; Tabs nil
  (set-variable 'indent-tabs-mode nil)
  )

(add-hook 'web-mode-hook  'my-web-mode-hook)
;; resaltar columna actual
(setq web-mode-enable-current-column-highlight t)


;; para react
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
;;(setq package-selected-packages '(lsp-mode yasnippet lsp-treemacs helm-lsp projectile hydra flycheck company avy which-key helm-xref dap-mode zenburn-theme json-mode))
(setq package-selected-packages '(yasnippet projectile hydra flycheck company avy which-key dap-mode zenburn-theme json-mode))
(when (cl-find-if-not #'package-installed-p package-selected-packages)
  (package-refresh-contents)
  (mapc #'package-install package-selected-packages))
;;(load-theme 'zenburn t)
;; (helm-mode)
;; (require 'helm-xref)
;;(define-key global-map [remap find-file] #'helm-find-files)
;;(define-key global-map [remap execute-extended-command] #'helm-M-x)
;;(define-key global-map [remap switch-to-buffer] #'helm-mini)
(which-key-mode)
;(add-hook 'prog-mode-hook #'lsp)
(setq gc-cons-threshold (* 100 1024 1024)
      read-process-output-max (* 1024 1024)
      company-idle-delay 0.0
      company-minimum-prefix-length 1
      create-lockfiles nil ;; lock files will kill `npm start'
      )
      ;;lsp-headerline-breadcrumb-enable t)
(with-eval-after-load 'lsp-mode
  (require 'dap-chrome)
  ;(add-hook 'lsp-mode-hook #'lsp-enable-which-key-integration)
  (yas-global-mode))

;; Para typescript mode
(require 'ansi-color)
(defun colorize-compilation-buffer ()
  (ansi-color-apply-on-region compilation-filter-start (point-max)))
(add-hook 'compilation-filter-hook 'colorize-compilation-buffer)
;; Identación de 2 espacios
;; (defun dotspacemacs/user-config ()
;;   (setq-default typescript-indent-level 2))

;;;;; Para tide (typescript interactive development enviroment)
(defun setup-tide-mode ()
  (interactive)
  (tide-setup)
  (flycheck-mode +1)
  (setq flycheck-check-syntax-automatically '(save mode-enabled))
  (eldoc-mode +1)
  (tide-hl-identifier-mode +1)
  ;; company is an optional dependency. You have to
  ;; install it separately via package-install
  ;; `M-x package-install [ret] company`
  (company-mode +1))
;; aligns annotation to the right hand side
(setq company-tooltip-align-annotations t)
;; formats the buffer before saving
(add-hook 'before-save-hook 'tide-format-before-save)

(add-hook 'typescript-mode-hook #'setup-tide-mode)

;; asociando archivos con extensiones automaticamente a web-mode
(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.vue\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.scss\\'" . web-mode))

(setq web-mode-engines-alist
      '(("php"    . "\\.phtml\\'")
        ("blade"  . "\\.blade\\.")
	("vue"  . "\\.vue\\.")
	)
      )

;; agregando autcompletado de ac-js2 a js2-mode
;; (add-hook 'js-mode-hook 'js2-minor-mode)
;; (add-hook 'js2-mode-hook 'ac-js2-mode)
;; ;; js2-mode provides 4 level of syntax highlighting. They are

;; ;;     0 or a negative value means none.
;; ;;     1 adds basic syntax highlighting.
;; ;;     2 adds highlighting of some Ecma built-in properties.
;; ;;     3 adds highlighting of many Ecma built-in functions.
;; (setq js2-highlight-level 3)

;; activar ac-autocomplete en algunos modos por defecto
(require 'auto-complete)
(global-auto-complete-mode t)

(setq ac-modes
      '(auto-complete js2-mode rjsx-mode python	c-mode lisp-mode javascript-mode lisp-interaction-mode ruby-mode web-mode vue-mode jedi	typescript-mode	tide terraform-mode shell-script-mode))

;; quitando la barra de herramientas generica
(tool-bar-mode -1)

(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

; esto hace que los warnings de eso modo no se muestren por defecto
;; (js2-mode-hide-warnings-and-errors)

;; python
(add-hook 'python-mode-hook 'jedi:setup)
(setq jedi:complete-on-dot t)

;; (add-hook 'python-hook 'jedi:setup)
;; (setq jedi:complete-on-dot t)

;; para habilitar/desabilitar neotree con F8
(require 'neotree)
(global-set-key [f8] 'neotree-toggle)
;; desactivar autochange root
(setq neo-autorefresh nil)


;; para multiple cursor
(global-set-key (kbd "C-c m c") 'mc/edit-lines)
;; para marcar lineas basadas en 
;; When you want to add multiple cursors not based on continuous lines, but based on keywords in the buffer, use:
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
;; al hacer Ctrl+Shift+click se marca el cursor manualmente
(global-set-key (kbd "C-S-<mouse-1>") 'mc/add-cursor-on-click)
;; El comando insert numbers en múltiples cursores empieza en 1
(setq mc/insert-numbers-default 1)


;; ruby (auto completado de emacs-robe) (no parece funcionar)
(add-hook 'robe-mode-hook 'ac-robe-setup)

;; probando enh-ruby-mode
;(setq enh-ruby-program "/home/strymsg/.rbenv/versions/2.5.0/bin/ruby") ; so that still works if ruby points to ruby1.8

;; ------ highlight indentation ----
;; Sirve en modos como yaml
(defun my-highlight-indentation-mode-hook ()
  (highlight-indentation-mode))
(add-hook 'my-highlight-indentation-mode-hook 'python)

;; FONTS -------------------------------------
;; FONTS -------------------------------------
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :extend nil :stipple nil :background "black" :foreground "gray83" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight regular :height 108 :width normal :foundry "outlined" :family "DejaVu Sans Mono"))))
 '(ansi-color-italic ((t (:inherit italic :slant italic))))
 '(cursor ((t (:background "white"))))
 '(ein:cell-input-area ((t (:background "#222"))))
 '(ein:cell-output-area ((t (:background "gray69" :foreground "#121212"))) t)
 '(font-lock-comment-delimiter-face ((t (:inherit font-lock-comment-face :foreground "slate gray" :slant italic))))
 '(font-lock-comment-face ((t (:foreground "#499BB5"))))
 '(font-lock-constant-face ((t (:foreground "DodgerBlue2"))))
 '(font-lock-doc-face ((t (:foreground "#9D99D3"))))
 '(font-lock-function-name-face ((t (:foreground "#29A41F" :weight bold))))
 '(font-lock-keyword-face ((t (:foreground "#47BA72" :weight bold))))
 '(font-lock-preprocessor-face ((t (:foreground "#A4C441"))))
 '(font-lock-string-face ((t (:foreground "#F79362"))))
 '(font-lock-type-face ((t (:foreground "#BE23F3"))))
 '(font-lock-variable-name-face ((t (:foreground "#DFE026" :weight bold))))
 '(highlight-indentation-current-column-face ((t (:background "firebrick4" :distant-foreground "#5F376E"))))
 '(highlight-indentation-face ((t (:inherit fringe :background "#3F303D"))))
 '(italic ((t (:slant italic))))
 '(js2-function-call ((t (:foreground "#F48BC7"))))
 '(js2-warning ((t (:underline "#464712"))))
 '(link ((t (:foreground "green1" :underline t))))
 '(markdown-header-face ((t (:inherit font-lock-function-name-face :foreground "#E6CE8C" :weight bold :height 1.0))))
 '(markdown-header-face-1 ((t (:inherit nil :foreground "#E9B97D" :weight bold :height 1.6))))
 '(markdown-header-face-2 ((t (:inherit markdown-header-face :foreground "#4F9D69" :height 1.5))))
 '(markdown-header-face-3 ((t (:inherit markdown-header-face :foreground "yellow green" :height 1.4))))
 '(markdown-header-face-4 ((t (:inherit markdown-header-face :foreground "#C8796B" :height 1.3))))
 '(markdown-header-face-5 ((t (:inherit markdown-header-face :foreground "#DE6474" :height 1.2))))
 '(markdown-header-face-6 ((t (:inherit markdown-header-face :foreground "#E871BC" :height 1.1))))
 '(markdown-header-rule-face ((t (:inherit font-lock-function-name-face :foreground "#DBBD1C" :weight bold))))
 '(mode-line ((t (:background "grey12" :foreground "#3cb642" :box (:line-width -1 :style released-button)))))
 '(py-class-name-face ((t (:inherit font-lock-type-face :weight bold))))
 '(py-decorators-face ((t (:inherit font-lock-keyword-face :foreground "#E060CB"))))
 '(py-def-class-face ((t (:inherit font-lock-keyword-face :foreground "#7DE060"))))
 '(py-number-face ((t (:inherit default :foreground "peach puff"))))
 '(py-object-reference-face ((t (:inherit py-pseudo-keyword-face :foreground "#8AE999"))))
 '(py-try-if-face ((t (:inherit font-lock-keyword-face :foreground "#78eede"))))
 '(py-variable-name-face ((t (:inherit default :foreground "#EAE0A5"))))
 '(region ((t (:background "#24396e"))))
 '(web-mode-html-tag-bracket-face ((t (:foreground "#c5ea91")))))

(set-face-attribute 'default nil
		    :family "DejaVu Sans")

;; Set transparency of emacs
;; esta funcion debe ser llamada explicitamente
(defun transparency (value)
  "Sets the transparency of the frame window. 0=transparent/100=opaque"
  (interactive "nTransparency Value 0 - 100 opaque:")
  (set-frame-parameter (selected-frame) 'alpha value))

;;
(ac-config-default)

;; ---- personalizados kbd ----
;; pretify json usando python
;; fuente: https://axxllence.wordpress.com/2012/12/18/emacs-pretty-print-json/
(defun pretty-print-json(&optional b e)
  (interactive "r")
  (shell-command-on-region b e "python -m json.tool")
  )


;; Set transparency of emacs
;; esta funcion debe ser llamada explicitamente
(defun transparency (value)
  "Sets the transparency of the frame window. 0=transparent/100=opaque"
  (interactive "nTransparency Value 0 - 100 opaque:")
  (set-frame-parameter (selected-frame) 'alpha value))

;; ---- LaTeX ----
(latex-preview-pane-enable)

;; ---- plantuml ----
;; Sample jar configuration
(setq plantuml-jar-path "~/Misc/emacs/plantuml.jar")
(setq plantuml-default-exec-mode 'jar)

;; (set-face-background 'highlight-indentation-face "#2E2419")
;; (set-face-background 'highlight-indentation-current-column-face "#483E31")

;; --- python ---
;; Correct setup of elpy can be seen in https://elpy.readthedocs.io/en/latest/concepts.html
;; In may case what worked was one time:
;; M-x package-install RET elpy RET
;; elpy-rpc-reinstall-virtualenv
(elpy-enable)
(setq elpy-rpc-python-command "python3")
(setq elpy-rpc-backend "jedi")  

;; For getting the RPC process (seems like the context) of a virtualenv, should run
;; pyvenv-activate
;; then **choose** the virtualenv path
;; Default is ~/.emacs.d/elpy/rpc-venv/


;; flycheck
(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))

;; Enable autopep8
(require 'py-autopep8)
(add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)


;; hooks para dtrt-ident
;; (add-to-list dtrt-indent-hook-mapping-list
;;   ;; Mode      Syntax     Variable
;;   '((web-mode  js2-mode   web-mode-code-indent-offset))
;;   )

;; ---- fonts por modos ----
;; TODO: Buscar la forma de que el cambio de estilos solo se aplique a los buffers donde estan abiertos los modos
;;--------------------------------------------

;; -------------- scala metals -------------------
;; Enable defer and ensure by default for use-package
;; Keep auto-save/backup files separate from source code:  https://github.com/scalameta/metals/issues/1027
(setq use-package-always-defer t
      use-package-always-ensure t
      backup-directory-alist `((".*" . ,temporary-file-directory))
      auto-save-file-name-transforms `((".*" ,temporary-file-directory t)))

(use-package scala-mode
	     :interpreter ("scala" . scala-mode))

;; Enable sbt mode for executing sbt commands
(use-package sbt-mode
  :commands sbt-start sbt-command
  :config
  ;; WORKAROUND: https://github.com/ensime/emacs-sbt-mode/issues/31
  ;; allows using SPACE when in the minibuffer
  (substitute-key-definition
   'minibuffer-complete-word
   'self-insert-command
   minibuffer-local-completion-map)
   ;; sbt-supershell kills sbt-mode:  https://github.com/hvesalai/emacs-sbt-mode/issues/152
   (setq sbt:program-options '("-Dsbt.supershell=false")))

;; Enable nice rendering of diagnostics like compile errors.
(use-package flycheck
  :init (global-flycheck-mode))

(use-package lsp-mode
  ;; Optional - enable lsp-mode automatically in scala files
  ;; You could also swap out lsp for lsp-deffered in order to defer loading
  :hook  (scala-mode . lsp)
         (lsp-mode . lsp-lens-mode)
  :config
  ;; Uncomment following section if you would like to tune lsp-mode performance according to
  ;; https://emacs-lsp.github.io/lsp-mode/page/performance/
  ;; (setq gc-cons-threshold 100000000) ;; 100mb
  ;; (setq read-process-output-max (* 1024 1024)) ;; 1mb
  ;; (setq lsp-idle-delay 0.500)
  ;; (setq lsp-log-io nil)
  ;; (setq lsp-completion-provider :capf)
  (setq lsp-prefer-flymake nil)
  ;; Makes LSP shutdown the metals server when all buffers in the project are closed.
  ;; https://emacs-lsp.github.io/lsp-mode/page/settings/mode/#lsp-keep-workspace-alive
  (setq lsp-keep-workspace-alive nil))

;; Add metals backend for lsp-mode
(use-package lsp-metals)

;; Enable nice rendering of documentation on hover
;;   Warning: on some systems this package can reduce your emacs responsiveness significally.
;;   (See: https://emacs-lsp.github.io/lsp-mode/page/performance/)
;;   In that case you have to not only disable this but also remove from the packages since
;;   lsp-mode can activate it automatically.
(use-package lsp-ui)

;; lsp-mode supports snippets, but in order for them to work you need to use yasnippet
;; If you don't want to use snippets set lsp-enable-snippet to nil in your lsp-mode settings
;; to avoid odd behavior with snippets and indentation
(use-package yasnippet)

;; Use company-capf as a completion provider.
;;
;; To Company-lsp users:
;;   Company-lsp is no longer maintained and has been removed from MELPA.
;;   Please migrate to company-capf.
(use-package company
  :hook (scala-mode . company-mode)
  :config
  (setq lsp-completion-provider :capf))

;; Posframe is a pop-up tool that must be manually installed for dap-mode
(use-package posframe)

;; Use the Debug Adapter Protocol for running tests and debugging
(use-package dap-mode
  :hook
  (lsp-mode . dap-mode)
  (lsp-mode . dap-ui-mode))
;; -----------------------------------------------
